<?php
if (get_field('brand') == 'Karastan'){ ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_karastan.png" class="product-logo" />

<?php } elseif (get_field('brand') == 'SmartSolutions') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" />

<?php } elseif (get_field('brand') == 'STAINMASTER') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/stainmaster_logo.png" class="product-logo" />

<?php } elseif (get_field('brand') == 'Armstrong') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/armstronglogo.png" class="product-logo" />

<?php } elseif (get_field('brand') == 'Quick-Step') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Quick-step-logo.png" class="product-logo" />
<?php } elseif (get_field('brand') == 'Daltile') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/daltile-logo.gif" class="product-logo" />
<?php } elseif (get_field('brand') == 'Floorscapes') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/floorscapes.jpg" class="product-logo" />
<?php } elseif (get_field('brand') == 'OpenLine') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" />
<?php } elseif (get_field('brand') == 'ColorCenter') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-color-center.png" class="product-logo" />
<?php } elseif (get_field('brand') == 'Mohawk ColorCenter') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-color-center.png" class="product-logo" />
<?php } elseif (get_field('brand') == 'Mohawk') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" />
<?php } elseif (get_field('brand') == 'Bruce') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bruce-logo.png" class="product-logo" />
<?php } elseif (get_field('brand') == 'NFA') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" />
<?php } elseif (get_field('brand') == 'Congoleum') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/congo_main_logo1.png" class="product-logo" />
<?php } elseif (get_field('brand') == 'Mannington') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/mannington-1.png" class="product-logo" />
<?php } elseif (get_field('brand') == 'IVC') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ivclogo.png" class="product-logo" />
<?php } elseif (get_field('brand') == 'Shaw') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/shaw-logo.png" class="product-logo" />
<?php } elseif (get_field('brand') == 'Shaw Tuftex') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/shaw-logo.png" class="product-logo" />
<?php } elseif (get_field('brand') == 'Emser') { ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/emser-logo.png" class="product-logo" />

<?php } else { ?>
    <?php the_field('brand'); ?>
<?php } ?>